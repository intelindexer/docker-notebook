#!/usr/bin/env sh
set -e

for f in /docker-entrypoint.d/*; do
    if [ ! -f "$f" ]; then
        continue
    fi

    case "$f" in
        *.sh)
            echo "==> running $f";
            "$f";;

        *.py)
            echo "==> running: $f";
            python "$f";;
        *);;
    esac
done

echo "==> running: $@"
exec "$@"
